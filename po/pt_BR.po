# Brazilian Portuguese translation for reminders-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the reminders-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: reminders-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-28 22:28+0200\n"
"PO-Revision-Date: 2020-02-11 02:21+0000\n"
"Last-Translator: Rodrigo Benedito <rodrigojob@yahoo.com.br>\n"
"Language-Team: Portuguese (Brazil) <https://translate.ubports.com/projects/"
"ubports/notes-app/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:24+0000\n"

#: com.ubuntu.reminders.desktop.in.in.h:1 src/app/qml/Reminders.qml:559
msgid "Notes"
msgstr "Notas"

#: com.ubuntu.reminders.desktop.in.in.h:2
msgid "Ubuntu Notes app, powered by Evernote"
msgstr "Aplicativo de Notas Ubuntu, provido por Evernote"

#: src/app/qml/Reminders.qml:100 src/app/qml/Reminders.qml:263
#: src/app/qml/Reminders.qml:268 src/app/qml/components/NoteHeader.qml:52
#: src/app/qml/components/PageWithBottomEdge.qml:123
#: src/app/qml/ui/EditNoteView.qml:42 src/app/qml/ui/NotesPage.qml:68
msgid "Untitled"
msgstr "Sem título"

#: src/app/qml/Reminders.qml:419
msgid "Default notebook"
msgstr "Caderno padrão"

#: src/app/qml/Reminders.qml:537
msgid "Select note to attach imported file"
msgstr "Selecionar nota para anexar arquivo importado"

#: src/app/qml/Reminders.qml:537
msgid "Select note to attach imported files"
msgstr "Selecionar nota para anexar arquivo importado"

#: src/app/qml/Reminders.qml:587
msgid "Notebooks"
msgstr "Blocos de notas"

#: src/app/qml/Reminders.qml:621
msgid "Reminders"
msgstr "Lembretes"

#: src/app/qml/Reminders.qml:638 src/app/qml/components/NotesDelegate.qml:80
msgid "Tags"
msgstr "Tags"

#: src/app/qml/Reminders.qml:651
msgid "Accounts"
msgstr "Contas"

#: src/app/qml/Reminders.qml:670
msgid ""
"No note selected.\n"
"Select a note to see it in detail."
msgstr ""
"Nenhuma nota selecionada.\n"
"Selecione uma nota para ver seus detalhes."

#: src/app/qml/Reminders.qml:713
msgid "Sync with Evernote"
msgstr "Sincronizar com Evernote"

#: src/app/qml/Reminders.qml:714
msgid "Notes can be stored on this device, or optionally synced with Evernote."
msgstr ""
"As notas podem ser armazenadas neste dispositivo ou, opcionalmente, "
"sincronizadas com o Evernote."

#: src/app/qml/Reminders.qml:715
msgid "To sync with Evernote, you need an Evernote account."
msgstr ""
"Para sincronizar com o Evernote você precisará de uma conta no Evernote."

#: src/app/qml/Reminders.qml:730
msgid "Not Now"
msgstr "Agora Não"

#: src/app/qml/Reminders.qml:739
msgid "Set Up…"
msgstr "Configuração…"

#: src/app/qml/Reminders.qml:754
#, qt-format
msgid "Importing %1 items"
msgstr "Importanto %1 itens"

#: src/app/qml/Reminders.qml:755
msgid "Importing 1 item"
msgstr "Importando 1 item"

#: src/app/qml/Reminders.qml:757
msgid ""
"Do you want to create a new note for those items or do you want to attach "
"them to an existing note?"
msgstr ""
"Deseja criar uma nova nota para esses itens ou deseja anexá-los a uma nota "
"existente?"

#: src/app/qml/Reminders.qml:758
msgid ""
"Do you want to create a new note for this item or do you want to attach it "
"to an existing note?"
msgstr ""
"Deseja criar uma nova nota para esse item ou deseja anexá-lo a uma nota "
"existente?"

#: src/app/qml/Reminders.qml:764
msgid "Create new note"
msgstr "Criar uma nova nota"

#: src/app/qml/Reminders.qml:769
msgid "Attach to existing note"
msgstr "Anexar à uma nota existente"

#: src/app/qml/Reminders.qml:774
msgid "Cancel import"
msgstr "Cancelar importação"

#: src/app/qml/components/ConflictDelegate.qml:35
msgid "Notebook:"
msgstr "Caderno:"

#: src/app/qml/components/ConflictDelegate.qml:63
msgid "Reminder:"
msgstr "Lembrete:"

#: src/app/qml/components/EditTagsDialog.qml:29
msgid "Edit tags"
msgstr "Editar etiquetas"

#: src/app/qml/components/EditTagsDialog.qml:32
msgid "Enter a tag name to attach it to the note."
msgstr "Insira uma etiqueta para anexar à nota."

#: src/app/qml/components/EditTagsDialog.qml:33
msgid "Enter a tag name or select one from the list to attach it to the note."
msgstr "Insira uma etiqueta ou selecione uma da lista para anexá-la à nota."

#: src/app/qml/components/EditTagsDialog.qml:61
msgid "Tag name"
msgstr "Nome da etiqueta"

#: src/app/qml/components/EditTagsDialog.qml:146
#: src/app/qml/ui/NotebooksPage.qml:213 src/app/qml/ui/TagsPage.qml:147
msgid "OK"
msgstr "OK"

#. TRANSLATORS: Button to close the edit mode
#. TRANSLATORS: Button to close the note viewer
#: src/app/qml/components/EditTagsDialog.qml:202
#: src/app/qml/ui/EditNoteView.qml:604 src/app/qml/ui/NoteView.qml:150
msgid "Close"
msgstr "Fechar"

#. TRANSLATORS: Button that deletes a reminder
#: src/app/qml/components/NotebooksDelegate.qml:36
#: src/app/qml/components/NotesDelegate.qml:53
#: src/app/qml/components/TagsDelegate.qml:34 src/app/qml/ui/NotesPage.qml:90
#: src/app/qml/ui/SetReminderView.qml:76
msgid "Delete"
msgstr "Excluir"

#: src/app/qml/components/NotebooksDelegate.qml:97
#, qt-format
msgid "Last edited %1"
msgstr "Última edição em %1"

#: src/app/qml/components/NotebooksDelegate.qml:116
msgid "Shared"
msgstr "Compartilhada"

#: src/app/qml/components/NotebooksDelegate.qml:116
msgid "Private"
msgstr "Particular"

#: src/app/qml/components/NotesDelegate.qml:73
msgid "Reminder"
msgstr "Lembrete"

#. TRANSLATORS: Button to go from note viewer to note editor
#: src/app/qml/components/NotesDelegate.qml:87 src/app/qml/ui/NoteView.qml:172
#: src/app/qml/ui/NotesPage.qml:112
msgid "Edit"
msgstr "Editar"

#: src/app/qml/components/PulldownListView.qml:40
msgid "Release to refresh"
msgstr "Soltar para atualizar"

#: src/app/qml/components/PulldownListView.qml:40
msgid "Pull down to refresh"
msgstr "Puxe para baixo para atualizar"

#: src/app/qml/components/RemindersDelegate.qml:36
msgid "Clear reminder"
msgstr "Limpar lembrete"

#: src/app/qml/components/RemindersDelegate.qml:49
msgid "Mark as undone"
msgstr "Marcar como não realizado"

#: src/app/qml/components/RemindersDelegate.qml:49
msgid "Mark as done"
msgstr "Marcar como concluído"

#: src/app/qml/components/RemindersDelegate.qml:57
#: src/app/qml/ui/NotesPage.qml:98
msgid "Edit reminder"
msgstr "Editar lembrete"

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:25
msgid "Resolve conflict"
msgstr "Resolver conflito"

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:30
msgid ""
"This will <b>keep the changes made on this device</b> and <b>restore the "
"note on Evernote</b>."
msgstr ""
"Isso <b> manterá as alterações feitas neste dispositivo </b> e <b> "
"restaurará a nota no Evernote </b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:32
msgid "This will <b>delete the changed note on Evernote</b>."
msgstr "Isto <b>removerá as mudanças da nota no Evernote</b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:34
msgid ""
"This will <b>keep the changes made on this device</b> and <b>discard any "
"changes made on Evernote</b>."
msgstr ""
"Isso <b> manterá as alterações feitas neste dispositivo </b> e <b> "
"descartará quaisquer alterações feitas no Evernote </b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:38
msgid "This will <b>delete the changed note on this device</b>."
msgstr "Isso <b> removerá as alterações feitas neste dispositivo </b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:40
msgid ""
"This will <b>download the changed note from Evernote</b> and <b>restore it "
"on this device</b>."
msgstr ""
"Isso <b> baixará as alterações feitas do Evernote </b> e <b> Alterará a nota "
"deste dispositivo</b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:42
msgid ""
"This will <b>download the changed note from Evernote</b> and <b>discard any "
"changes made on this device</b>."
msgstr ""
"Isso <b> baixará as alterações do Evernote </b> e <b> descartará quaisquer "
"alterações feitas neste dispositivo</b>."

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:45
msgid "Are you sure you want to continue?"
msgstr "Você tem certeza que deseja continuar?"

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:57
msgid "Yes"
msgstr "Sim"

#: src/app/qml/components/ResolveConflictConfirmationDialog.qml:66
msgid "No"
msgstr "Não"

#: src/app/qml/components/SortingDialog.qml:9
msgid "Sort by"
msgstr "Ordenar por"

#: src/app/qml/components/SortingDialog.qml:19
msgid "Date created (newest first)"
msgstr "Data de criação (mais recente primeiro)"

#: src/app/qml/components/SortingDialog.qml:20
msgid "Date created (oldest first)"
msgstr "Data de criação (antigo primeiro)"

#: src/app/qml/components/SortingDialog.qml:21
msgid "Date updated (newest first)"
msgstr "Data de alteração (recente primeiro)"

#: src/app/qml/components/SortingDialog.qml:22
msgid "Date updated (oldest first)"
msgstr "Data de alteração (antigo primeiro)"

#: src/app/qml/components/SortingDialog.qml:23
msgid "Title (ascending)"
msgstr "Título (crescente)"

#: src/app/qml/components/SortingDialog.qml:24
msgid "Title (descending)"
msgstr "Título (decrescente)"

#: src/app/qml/ui/AccountSelectorPage.qml:29
msgid "Select account"
msgstr "Selecionar conta"

#: src/app/qml/ui/AccountSelectorPage.qml:44
msgid "Store notes locally only"
msgstr "Armazenar notas somente localmente"

#: src/app/qml/ui/AccountSelectorPage.qml:52
msgid "Accounts on www.evernote.com"
msgstr "Contas em www.evernote.com"

#: src/app/qml/ui/AccountSelectorPage.qml:82
#, qt-format
msgid "%1 - Tap to authorize"
msgstr "%1 - Toque para autorizar"

#: src/app/qml/ui/AccountSelectorPage.qml:91
msgid "Add new account"
msgstr "Adicionar nova conta"

#: src/app/qml/ui/EditNoteView.qml:424
msgid "Font"
msgstr "Fonte"

#: src/app/qml/ui/EditNoteView.qml:436
msgid "Size"
msgstr "Tamanho"

#. TRANSLATORS: Toolbar button for "Bold"
#: src/app/qml/ui/EditNoteView.qml:459
msgid "B"
msgstr "N"

#. TRANSLATORS: Toolbar button for "Italic"
#: src/app/qml/ui/EditNoteView.qml:472
msgid "I"
msgstr "I"

#. TRANSLATORS: Toolbar button for "Underline"
#: src/app/qml/ui/EditNoteView.qml:486
msgid "U"
msgstr "S"

#. TRANSLATORS: Toolbar button for "Strikeout"
#: src/app/qml/ui/EditNoteView.qml:500
msgid "T"
msgstr "T"

#: src/app/qml/ui/NoteConflictPage.qml:27
msgid "Conflicting changes"
msgstr "Alterações conflitantes"

#: src/app/qml/ui/NoteConflictView.qml:54
msgid ""
"This note has been modified in multiple places. Open each version to check "
"the content, then swipe to delete one of them."
msgstr ""
"Esta nota foi modificada em vários lugares. Abra cada versão para verificar "
"o conteúdo e deslize para excluir uma delas."

#: src/app/qml/ui/NoteConflictView.qml:63
msgid "Deleted on this device:"
msgstr "Apagado nesse dispositivo:"

#: src/app/qml/ui/NoteConflictView.qml:63
msgid "Modified on this device:"
msgstr "Modificado nesse dispositivo:"

#: src/app/qml/ui/NoteConflictView.qml:85
msgid "Deleted from Evernote:"
msgstr "Apagado do Evernote:"

#: src/app/qml/ui/NoteConflictView.qml:85
msgid "Modified somewhere else:"
msgstr "Modificado por outra pessoa:"

#: src/app/qml/ui/NoteView.qml:184
msgid "Attachment"
msgstr "Anexo"

#: src/app/qml/ui/NotebooksPage.qml:45
msgid "Add notebook"
msgstr "Adicionar bloco de notas"

#: src/app/qml/ui/NotebooksPage.qml:52 src/app/qml/ui/NotesPage.qml:83
#: src/app/qml/ui/RemindersPage.qml:36 src/app/qml/ui/SearchNotesPage.qml:60
#: src/app/qml/ui/TagsPage.qml:52
msgid "Search"
msgstr "Pesquisar"

#: src/app/qml/ui/NotebooksPage.qml:166
msgid "Cancel"
msgstr "Cancelar"

#. TRANSLATORS: Button that saves a reminder
#: src/app/qml/ui/NotebooksPage.qml:178 src/app/qml/ui/SetReminderView.qml:88
msgid "Save"
msgstr "Salvar"

#: src/app/qml/ui/NotebooksPage.qml:199
msgid "Rename notebook"
msgstr "Renomear caderno"

#: src/app/qml/ui/NotebooksPage.qml:200
#, qt-format
msgid "Enter a new name for notebook %1"
msgstr "Informe um novo nome para o caderno %1"

#: src/app/qml/ui/NotebooksPage.qml:209 src/app/qml/ui/TagsPage.qml:143
msgid "Name cannot be empty"
msgstr "O nome não pode ser vazio"

#: src/app/qml/ui/NotesPage.qml:44 src/app/qml/ui/NotesPage.qml:65
msgid "Add note"
msgstr "Adicionar nota"

#: src/app/qml/ui/NotesPage.qml:73
msgid "Sorting"
msgstr "Classificação"

#: src/app/qml/ui/NotesPage.qml:98
msgid "Set reminder"
msgstr "Configurar lembrete"

#: src/app/qml/ui/NotesPage.qml:249
msgid ""
"No notes available. You can create new notes by pulling the note editor up "
"from the bottom edge."
msgstr ""
"Sem nota disponível. Você pode criar uma nota nova puxando o editor de notas "
"para cima a partir da borda inferior."

#: src/app/qml/ui/RemindersPage.qml:97
msgid ""
"No reminders available. You can create new reminders by setting a reminder "
"when viewing a note."
msgstr ""
"Não há lembretes disponíveis. Você pode criar novos lembretes, definindo um "
"lembrete ao visualizar uma nota."

#: src/app/qml/ui/SearchNotesPage.qml:32
msgid "Search notes"
msgstr "Pesquisar notas"

#: src/app/qml/ui/SetReminderView.qml:36
msgid "Select date and time for the reminder:"
msgstr "Selecionar data e hora para o lembrete:"

#. TRANSLATORS: A checkbox with marks the reminder as done
#: src/app/qml/ui/SetReminderView.qml:64
msgid "Reminder done"
msgstr "Lembrete concluído"

#: src/app/qml/ui/TagsPage.qml:126
msgid "No tags available. You can tag notes while viewing them."
msgstr ""
"Não há tags disponíveis. Você pode criar tags em notas enquanto os visualiza."

#: src/app/qml/ui/TagsPage.qml:133
msgid "Rename tag"
msgstr "Renomear tag"

#: src/app/qml/ui/TagsPage.qml:134
#, qt-format
msgid "Enter a new name for tag %1"
msgstr "Informe um nome para a tag %1"

#: src/libqtevernote/evernoteconnection.cpp:268
msgid "Offline mode"
msgstr "Modo off-line"

#: src/libqtevernote/evernoteconnection.cpp:273
#: src/libqtevernote/evernoteconnection.cpp:362
msgid "Unknown error connecting to Evernote."
msgstr "Erro desconhecido ao conectar ao Evernote."

#: src/libqtevernote/evernoteconnection.cpp:286
msgid ""
"Error connecting to Evernote: Server version does not match app version. "
"Please update the application."
msgstr ""
"Erro ao conectar ao Evernote: Versão do servidor não confere com a versão do "
"aplicativo. Por favor, atualize a aplicação."

#: src/libqtevernote/evernoteconnection.cpp:292
#: src/libqtevernote/evernoteconnection.cpp:297
#, qt-format
msgid "Error connecting to Evernote: Error code %1"
msgstr "Erro ao conectar ao Evernote: Código de erro %1"

#: src/libqtevernote/evernoteconnection.cpp:302
msgid ""
"Error connecting to Evernote: Cannot download version information from "
"server."
msgstr ""
"Erro ao conectar ao Evernote: Não é possível baixar do servidor a informação "
"da versão."

#: src/libqtevernote/evernoteconnection.cpp:307
msgid "Unknown error connecting to Evernote"
msgstr "Erro desconhecido ao conectar ao Evernote"

#: src/libqtevernote/evernoteconnection.cpp:321
msgid "Error connecting to Evernote: Cannot download server information."
msgstr ""
"Erro ao conectar ao Evernote: Não é possível baixar a informação do servidor."

#: src/libqtevernote/evernoteconnection.cpp:329
#: src/libqtevernote/notesstore.cpp:1789
msgid ""
"Authentication for Evernote server expired. Please renew login information "
"in the accounts settings."
msgstr ""
"A Autenticação do servidor do Evernote expirou. por favor refazer o login "
"nas configurações da conta."

#: src/libqtevernote/evernoteconnection.cpp:332
#: src/libqtevernote/evernoteconnection.cpp:350
#, qt-format
msgid "Unknown error connecting to Evernote: %1"
msgstr "Erro desconhecido ao conectar ao Evernote: %1"

#: src/libqtevernote/evernoteconnection.cpp:341
msgid ""
"Error connecting to Evernote: Rate limit exceeded. Please try again later."
msgstr ""
"Erro de conexão ao Evernote. Limite de taxa excedido. Por favor, tente "
"novamente mais tarde."

#: src/libqtevernote/evernoteconnection.cpp:357
msgid ""
"Error connecting to Evernote: Connection failure when downloading server "
"information."
msgstr ""
"Erro ao conectar ao Evernote: Falha de conexão ao baixar a informação do "
"servidor."

#: src/libqtevernote/evernoteconnection.cpp:383
msgid "Error connecting to Evernote: Connection failure"
msgstr "Erro ao conectar ao Evernote: Falha na conexão"

#: src/libqtevernote/evernoteconnection.cpp:387
msgid "Unknown Error connecting to Evernote"
msgstr "Erro desconhecido ao conectar ao Evernote"

#: src/libqtevernote/evernoteconnection.cpp:435
#: src/libqtevernote/evernoteconnection.cpp:499
msgid "Disconnected from Evernote."
msgstr "Desconectado do Evernote."

#: src/libqtevernote/note.cpp:162 src/libqtevernote/note.cpp:196
#: src/libqtevernote/note.cpp:381
msgid "Today"
msgstr "Hoje"

#: src/libqtevernote/note.cpp:165 src/libqtevernote/note.cpp:199
msgid "Yesterday"
msgstr "Ontem"

#: src/libqtevernote/note.cpp:168 src/libqtevernote/note.cpp:202
msgid "Last week"
msgstr "Semana passada"

#: src/libqtevernote/note.cpp:171 src/libqtevernote/note.cpp:205
msgid "Two weeks ago"
msgstr "Duas semanas atrás"

#. TRANSLATORS: the first argument refers to a month name and the second to a year
#: src/libqtevernote/note.cpp:175 src/libqtevernote/note.cpp:209
#, qt-format
msgid "%1 %2"
msgstr "%1 %2"

#: src/libqtevernote/note.cpp:369
msgid "Done"
msgstr "Concluído"

#: src/libqtevernote/note.cpp:375
msgid "No date"
msgstr "Sem data"

#: src/libqtevernote/note.cpp:378
msgid "Overdue"
msgstr "Em atraso"

#: src/libqtevernote/note.cpp:384
msgid "Tomorrow"
msgstr "Amanhã"

#: src/libqtevernote/note.cpp:387
msgid "Next week"
msgstr "Próxima semana"

#: src/libqtevernote/note.cpp:390
msgid "In two weeks"
msgstr "Daqui a duas semanas"

#: src/libqtevernote/note.cpp:392
msgid "Later"
msgstr "Mais tarde"

#. TRANSLATORS: this is part of a longer string - "Last updated: today"
#: src/libqtevernote/notebook.cpp:122
msgid "today"
msgstr "hoje"

#. TRANSLATORS: this is part of a longer string - "Last updated: yesterday"
#: src/libqtevernote/notebook.cpp:126
msgid "yesterday"
msgstr "ontem"

#. TRANSLATORS: this is part of a longer string - "Last updated: last week"
#: src/libqtevernote/notebook.cpp:130
msgid "last week"
msgstr "semana passada"

#. TRANSLATORS: this is part of a longer string - "Last updated: two weeks ago"
#: src/libqtevernote/notebook.cpp:134
msgid "two weeks ago"
msgstr "duas semanas"

#. TRANSLATORS: this is used in the notes list to group notes created on the same month
#. the first parameter refers to a month name and the second to a year
#: src/libqtevernote/notebook.cpp:138
#, qt-format
msgid "on %1 %2"
msgstr "em %1 %2"

#: src/libqtevernote/notesstore.cpp:432
msgid ""
"This account is managed by Evernote. Use the Evernote website to delete "
"notebooks."
msgstr ""
"Esta conta é gerenciada pelo Evernote. Use o site do Evernote para excluir o "
"bloco de notas."

#: src/libqtevernote/notesstore.cpp:446
msgid ""
"Cannot delete the default notebook. Set another notebook to be the default "
"first."
msgstr ""
"Não é possível excluir o bloco de notas padrão. Defina outro bloco de notas "
"como o padrão primeiro."

#: src/libqtevernote/notesstore.cpp:1792
msgid "Rate limit for Evernote server exceeded. Please try again later."
msgstr ""
"O limite de taxa para o servidor Evernote foi excedido. Por favor, tente "
"novamente mais tarde."

#: src/libqtevernote/notesstore.cpp:1795
msgid "Upload quota for Evernote server exceed. Please try again later."
msgstr ""
"A cota de upload para o servidor Evernote excedeu. Por favor, tente "
"novamente mais tarde."

#: src/libqtevernote/notesstore.cpp:1834
msgid ""
"This account is managed by Evernote. Please use the Evernote website to "
"delete tags."
msgstr ""
"Esta conta é gerenciada pelo Evernote. Por favor use o site do Evernote  "
"para apagar as etiquetas."

#. TRANSLATORS: A default file name if we don't get one from the server. Avoid weird characters.
#: src/libqtevernote/resource.cpp:39
msgid "Unnamed"
msgstr "Sem nome"
